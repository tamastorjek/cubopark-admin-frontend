import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';

@Component({
  selector: 'app-search-input',
  templateUrl: './search-input.component.html',
  styleUrls: ['./search-input.component.sass']
})
export class SearchInputComponent implements OnInit {
  private escape = /[\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g;
  private searchInProgress = false;
  private previousResults = false;
  private searchTerm = '';
  private previousSearchTerm = '';
  private currentItem: any = null;
  private itemChanged = false;
  private inputWasCleared = false;
  @Input() autocomplete = false;
  @Input() items: Array<any>;
  @Input() bindValue = 'id';
  @Input() bindLabel = 'label';
  @Input() bindToExternal = false;
  @Input() showAllOnEmpty = false;
  @Input() maxResults = 10;
  @Input() placeholder = 'Search';

  public showResults = false;
  public results: Array<any> = [];
  @Output() externalResults = new EventEmitter<any[]>();

  @Output() valueChange = new EventEmitter<any>();
  @Output() selectionChange = new EventEmitter<any>();

  @ViewChild('searchInput', {static: false}) searchInput;

  constructor() {
  }

  ngOnInit() {
  }

  get term(): string {
    return this.searchTerm;
  }

  set term(val) {
    this.searchTerm = val;

    if (this.itemChanged) {
      this.itemChanged = false;
      return;
    }

    this.search(val);
  }

  @Input()
  get value(): any {
    return this.currentItem;
  }

  set value(val: any) {
    if (val === '') {
      val = null;
    }

    if (val !== this.currentItem) {
      this.currentItem = val;
      this.valueChange.emit(val);
    }
  }

  public hideList() {
    this.showResults = false;
  }

  public showList() {
    this.showResults = true;
  }

  private search(val) {
    if (this.searchInProgress) {
      return;
    }

    this.searchInProgress = true;

    if (!val && this.autocomplete && !this.bindToExternal) {
      this.results = this.items.slice(0, this.maxResults);
      this.previousResults = false;
      this.searchInProgress = false;
      this.showList();
      return;
    }

    if (!val || String(val).length < 2) {
      if (!this.previousResults) {
        this.searchInProgress = false;
        this.hideList();
        return;
      }

      this.previousResults = false;

      if (this.autocomplete && !this.bindToExternal) {
        this.hideList();
        this.results = [];
        this.searchInProgress = false;
        return;
      }

      this.results = this.showAllOnEmpty ? this.items : [];

      if (this.bindToExternal) {
        this.externalResults.emit(this.results);
      }

      this.searchInProgress = false;
      return;
    }

    this.results = [];

    const rx = new RegExp(val.replace(this.escape), 'i');

    for (const item of this.items) {
      if (item.disabled) {
        continue;
      }

      if (rx.test(item[this.bindLabel])) {
        this.results.push(item);

        if (this.maxResults && this.results.length === this.maxResults) {
          break;
        }
      }
    }

    if (this.bindToExternal) {
      this.externalResults.emit(this.results);
    }

    if (this.autocomplete && !this.bindToExternal) {
      this.showList();
    }

    this.previousResults = true;
    this.searchInProgress = false;
  }

  public selectItem(item: any) {
    this.value = this.bindValue === null ? item : item[this.bindValue];
    this.itemChanged = true;
    this.term = item[this.bindLabel];
    this.selectionChange.emit(this.value);
    this.hideList();
  }

  public onInputFocus(e) {
    // prevent triggering focus event from listener
    if (this.inputWasCleared) {
      this.inputWasCleared = false;
      return;
    }

    let input;

    this.previousSearchTerm = this.term;

    if (!e) {
      this.inputWasCleared = true;
      input = this.searchInput.nativeElement.querySelector('input');
      input.focus();
    }

    if (!this.term) {
      if (this.autocomplete && !this.bindToExternal) {
        const results = [];

        for (const item of this.items) {
          if (item.disabled) {
            continue;
          }

          results.push(item);

          if (results.length === this.maxResults) {
            break;
          }
        }

        this.results = results;
      } else {
        return;
      }
    }

    if (this.results.length && this.autocomplete && !this.bindToExternal) {
      this.showList();
    }

    if (!e) {
      return;
    }

    if (e.target.tagName === 'input') {
      e.target.select();
      return;
    }

    input = e.target.querySelector('input');

    input.focus();
    input.select();
  }

  public onClearInput() {
    this.term = '';
    this.value = null;
    this.itemChanged = true;
    this.selectionChange.emit(this.value);
    this.onInputFocus(null);
  }
}
