import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';

import {IonicModule} from '@ionic/angular';

import {ModalSearchbarComponent} from './modal-searchbar.component';
import {ModalSearchComponentModule} from '../modal-search/modal-search.module';
import {ModalSearchComponent} from '../modal-search/modal-search.component';

@NgModule({
  imports: [CommonModule, FormsModule, IonicModule, ModalSearchComponentModule],
  declarations: [ModalSearchbarComponent],
  exports: [ModalSearchbarComponent],
  entryComponents: [ModalSearchComponent]
})
export class ModalSearchbarComponentModule {
}
