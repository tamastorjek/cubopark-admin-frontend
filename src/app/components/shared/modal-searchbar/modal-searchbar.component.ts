import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Observable} from 'rxjs';
import {ModalController} from '@ionic/angular';
import {ModalSearchComponent} from '../modal-search/modal-search.component';
import {ModalSearchResult} from '../modal-search/modal-search-result.model';

@Component({
  selector: 'cp-modal-searchbar',
  templateUrl: './modal-searchbar.component.html',
  styleUrls: ['./modal-searchbar.component.scss'],
})
export class ModalSearchbarComponent implements OnInit {
  private modal: HTMLIonModalElement;

  @Input() source: Observable<any>;
  @Input() key = 'id';
  @Input() label = 'label';
  @Input() placeholder = 'Search';
  @Input() selection: ModalSearchResult = new ModalSearchResult();

  @Output() selectionChange = new EventEmitter<any>();

  constructor(
    public modalController: ModalController
  ) {
  }

  ngOnInit() {
  }

  onClick(e: any): void {
    // get the current value of the input
    // const value = e.target.tagName === 'INPUT' ? e.target.value : e.target.querySelector('input').value;
    this.presentModal();
  }

  private async presentModal() {
    if (this.modal) {
      return;
    }

    this.modal = await this.modalController.create({
      component: ModalSearchComponent,
      componentProps: {
        selection: this.selection,
        source: this.source,
        key: this.key,
        label: this.label,
        placeholder: this.placeholder
      },
      animated: false
    });

    this.modal.onDidDismiss()
      .then(event => this.onSelect(event.data));

    return await this.modal.present();
  }

  onSelect(result: ModalSearchResult): void {
    this.selection = result;
    this.modal = null;
    this.selectionChange.emit(result);
  }
}
