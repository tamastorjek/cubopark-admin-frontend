import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {IonicModule} from '@ionic/angular';

import {ModalSearchComponent} from './modal-search.component';
import {SearchbarComponentModule} from '../searchbar/searchbar.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SearchbarComponentModule
  ],
  declarations: [ModalSearchComponent],
  exports: [ModalSearchComponent]
})
export class ModalSearchComponentModule {
}
