import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Observable} from 'rxjs';
import {SearchbarResult} from '../searchbar/searchbar-result.model';
import {ModalController} from '@ionic/angular';
import {ModalSearchResult} from './modal-search-result.model';

@Component({
  selector: 'cp-modal-search',
  templateUrl: './modal-search.component.html',
  styleUrls: ['./modal-search.component.scss'],
})
export class ModalSearchComponent implements OnInit {
  private searchResults: SearchbarResult = new SearchbarResult();

  @Input() source: Observable<any>;
  @Input() key = 'id';
  @Input() label = 'label';
  @Input() placeholder = 'Search';
  @Input() selection: ModalSearchResult;

  constructor(private modalController: ModalController) {
  }

  ngOnInit() {
  }

  /**
   * Save results and save search term in state
   *
   * @param results Search results
   */
  onResultChange(results: SearchbarResult): void {
    this.searchResults = results;
  }

  /**
   * Check if the given item ID is in the search results
   *
   * @param id Item ID
   */
  inSearchResults(id: any): boolean {
    if (this.searchResults.results === null) {
      return true;
    }

    return this.searchResults.results.find(itemId => itemId === id) !== undefined;
  }

  closeModal(): void {
    this.modalController.dismiss(this.selection);
  }

  selectItem(label: string, key: any) {
    this.selection = new ModalSearchResult(label, key, this.searchResults.term);
    this.closeModal();
  }

  clear(): void {
    this.selection = new ModalSearchResult();
    this.closeModal();
  }
}
