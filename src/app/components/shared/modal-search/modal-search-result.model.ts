export class ModalSearchResult {
  label: string;
  key: any;
  term: string;

  constructor(label: string = '', key: any = null, term: string = '') {
    this.label = label;
    this.key = key;
    this.term = term;
  }
}
