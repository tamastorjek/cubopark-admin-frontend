import {ChangeDetectorRef, Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ImageCompressorService} from '../../../services/image-compressor.service';
import {LoaderService} from '../../../services/loader.service';
import {take} from 'rxjs/operators';
import {FileData} from '../../../models/file-data';

@Component({
  selector: 'cp-image-uploader',
  templateUrl: './image-uploader.component.html',
  styleUrls: ['./image-uploader.component.scss'],
})
export class ImageUploaderComponent implements OnInit {

  @Input() title = '';
  @Input() extraTypes: string[] = [];
  @Output() statusChange = new EventEmitter<File>();

  fileData: FileData = new FileData();

  public accepts = ['.png', '.jpg', '.jpeg'];

  constructor(
    private image: ImageCompressorService,
    private loader: LoaderService,
    private changeDetector: ChangeDetectorRef
  ) {
  }

  ngOnInit() {
    this.accepts = [...this.accepts, ...this.extraTypes];
  }

  process(event) {
    // no file was selected
    if (!event.target.files.length) {
      this.reset();
      return;
    }

    // get selected file's extension
    const fileExtension = event.target.files[0].name.split('.').pop().toLowerCase();

    // invalid file extension
    if (this.accepts.indexOf('.' + fileExtension) === -1) {
      // this.notification.alert('Invalid file type!');
      this.reset();
      return;
    }

    // not an image, simply assign
    if (this.extraTypes.indexOf('.' + fileExtension) !== -1) {
      this.fileData = new FileData({
        file: event.target.files[0],
        extension: fileExtension
      });
      return;
    }

    this.loader.show('image-upload-meter-reading');

    this.image.compress(event.target.files[0], 1000)
      .pipe(take(1))
      .subscribe(response => {
        const reader = new FileReader();
        reader.readAsDataURL(response.file);

        reader.onload = () => {
          response.extension = 'jpg';
          response.url = reader.result;

          this.setFile(response);

          setTimeout(() => this.loader.hide('image-upload-meter-reading'), 500);
        };
      });
  }

  private setFile(data: any): void {
    this.fileData = new FileData({
      file: data.file,
      extension: data.extension,
      size: {x: data.size[0], y: data.size[1]},
      url: data.url
    });

    this.changeDetector.detectChanges();
    this.statusChange.emit(data.file);
  }

  reset() {
    this.fileData = new FileData();
    this.changeDetector.detectChanges();
  }

  getBackground(): {} {
    if (this.fileData.url) {
      return {'background-image': 'url(' + this.fileData.url + ')'};
    }
  }

  hasFile(): boolean {
    return this.fileData.file !== undefined;
  }
}
