import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ImageUploaderComponent} from './image-uploader.component';
import {IonicModule} from '@ionic/angular';


@NgModule({
  declarations: [ImageUploaderComponent],
  imports: [
    CommonModule,
    IonicModule
  ],
  exports: [ImageUploaderComponent]
})
export class ImageUploaderModule {
}
