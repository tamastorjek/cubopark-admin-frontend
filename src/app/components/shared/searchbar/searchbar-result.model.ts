export class SearchbarResult {
  term: string;
  results: any[];

  constructor(term: string = '', results: any[] = null) {
    this.term = term;
    this.results = results;
  }
}
