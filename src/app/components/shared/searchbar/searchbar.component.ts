import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {BehaviorSubject, Observable, of} from 'rxjs';
import {search} from './search.operator';
import {SearchbarResult} from './searchbar-result.model';
import {ModalSearchComponent} from '../modal-search/modal-search.component';

@Component({
  selector: 'cp-searchbar',
  templateUrl: './searchbar.component.html',
  styleUrls: ['./searchbar.component.scss'],
})
export class SearchbarComponent implements OnInit {

  private term$: BehaviorSubject<string> = new BehaviorSubject<string>('');
  private results$: Observable<SearchbarResult>;

  public items: any[] = [];

  @Input() source: Observable<any>;
  @Input() key = 'id';
  @Input() label = 'label';
  @Input() placeholder = 'Search';
  @Input() value = '';

  @Output() resultChange = new EventEmitter<SearchbarResult>();

  constructor() {
  }

  ngOnInit() {
    // re-search items if they are changed
    this.source.subscribe(items => {
      this.items = items ? items : [];
      this.term$.next(this.term$.getValue());
    });

    // attach custom search operator to term
    this.results$ = this.term$.pipe(
      search(200, term => this.search(term))
    );

    // subscribe to result changes
    this.results$.subscribe(results => {
      // emit output if we have an external target
      this.resultChange.emit(results);
    });

    if (this.value) {
      this.term$.next(this.value);
    }
  }

  onChange(e: any): void {
    this.term$.next(e.target.value);
  }

  onFocus(): void {
    this.term$.next(this.term$.getValue());
  }

  onBlur(): void {
  }

  onClear(): void {
    this.term$.next('');
  }

  private search(term: string): Observable<SearchbarResult> {
    // on empty term show/hide results
    if (!term) {
      return of(new SearchbarResult('', null));
    }

    // start search
    // const rx = new RegExp(escapeRegExp(term), 'i');
    const rx = new RegExp(term, 'i');
    const results: any[] = [];

    for (const item of this.items) {
      if (rx.test(item[this.label])) {
        results.push(item[this.key]);
      }
    }

    return of(new SearchbarResult(term, results));
  }
}
