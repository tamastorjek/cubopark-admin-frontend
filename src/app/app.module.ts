import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {RouteReuseStrategy} from '@angular/router';

import {IonicModule, IonicRouteStrategy} from '@ionic/angular';

import {AppComponent} from './app.component';
import {AppRoutingModule} from './app-routing.module';
import {StoreModule} from '@ngrx/store';
import {reducers} from './store/reducers';
import {StoreRouterConnectingModule} from '@ngrx/router-store';
import {EffectsModule} from '@ngrx/effects';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {HttpClientInterceptor} from './services/http-client.interceptor';
import {ApiService} from './services/api.service';
import {metaReducers} from './store/reducers/meta.reducer';
import {LOCAL_STORAGE} from 'ngx-webstorage-service';
import {STATE_LOCAL_STORAGE, StateStorageService} from './services/state-storage.service';
import {StateService} from './services/state.service';
import {TenantEffects} from './store/effects/tenant.effects';
import {TenantService} from './services/tenant.service';

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [
    BrowserModule,
    IonicModule.forRoot(),
    AppRoutingModule,
    StoreModule.forRoot(reducers, {
      metaReducers,
      runtimeChecks: {
        strictStateImmutability: true,
        strictActionImmutability: true
      }
    }),
    StoreRouterConnectingModule.forRoot(),
    EffectsModule.forRoot([TenantEffects]),
    HttpClientModule
  ],
  providers: [
    {provide: RouteReuseStrategy, useClass: IonicRouteStrategy},
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpClientInterceptor,
      multi: true
    },
    {
      provide: STATE_LOCAL_STORAGE,
      useExisting: LOCAL_STORAGE
    },
    StateStorageService,
    ApiService,
    StateService,
    TenantService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(private stateService: StateService) {
  }
}
