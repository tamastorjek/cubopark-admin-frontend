export function toTitleCase(str: string): string {
  const parts = str.split('-');

  for (let i = 0, len = parts.length; i < len; i++) {
    parts[i] = parts[i].charAt(0).toUpperCase() + parts[i].slice(1);
  }

  return parts.join('');
}

export function toCamelCase(str: string): string {
  const parts = str.split('-');

  for (let i = 1, len = parts.length; i < len; i++) {
    parts[i] = parts[i].charAt(0).toUpperCase() + parts[i].slice(1);
  }

  return parts.join('');
}
