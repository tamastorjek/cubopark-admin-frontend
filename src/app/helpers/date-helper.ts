import * as dayjsPackage from 'dayjs';
export const dayjs = (dayjsPackage as any).default || dayjsPackage;
