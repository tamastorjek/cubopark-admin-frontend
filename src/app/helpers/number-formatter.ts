import {formatCurrency} from '@angular/common';

export function toFloat(value: number | string): number {
  if (!value) {
    return 0;
  }

  if (typeof value === 'string') {
    return parseFloat(value) / 100;
  }

  return value / 100;
}

export function toInt(value: number | string): number {
  if (!value) {
    return 0;
  }

  if (typeof value === 'string') {
    return Math.round(parseFloat(value) * 100);
  }

  return Math.round(value * 100);
}

export function toFloatCurrency(value: number): string {
  return formatCurrency(value / 100, 'en-US', 'RM', 'MYR');
}
