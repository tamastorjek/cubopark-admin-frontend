import {Injectable, isDevMode} from '@angular/core';
import {MeterReading} from '../models/meter-reading';
import {ApiService} from './api.service';
import {NotificationService} from './notification.service';
import {Observable, Subject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MeterReadingService {

  protected readingSavedSubject = new Subject<MeterReading>();

  constructor(private api: ApiService, private notification: NotificationService) {
  }

  get readingSaved(): Observable<MeterReading> {
    return this.readingSavedSubject.asObservable();
  }

  public store(reading: MeterReading) {
    const formData = new FormData();

    for (const field of Object.keys(reading)) {
      if (field === 'proof_file') {
        if (reading[field] !== null) {
          formData.append(field, reading[field], reading[field].name);
        }

        continue;
      }

      formData.append(field, reading[field]);
    }

    this.api.post('meter-readings', formData).subscribe((res) => {
      if (res.status === 'error') {
        if (isDevMode()) {
          console.log(res.messages);
        }
      } else {
        this.notification.toast('Meter reading saved.');
      }

      this.readingSavedSubject.next(res.data);
    });
  }
}
