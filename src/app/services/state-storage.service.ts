import {Inject, Injectable, InjectionToken} from '@angular/core';
import {StorageService} from 'ngx-webstorage-service';

export const STATE_LOCAL_STORAGE = new InjectionToken<StateStorageService>('STATE_LOCAL_STORAGE');

@Injectable({
  providedIn: 'root'
})
export class StateStorageService {

  constructor(@Inject(STATE_LOCAL_STORAGE) private storage: StorageService) {
  }

  public has(key: string): boolean {
    return this.storage.has(key);
  }

  public get(key: string, defaultValue: any = null): any {
    return this.has(key) ? this.storage.get(key) : (defaultValue !== null ? defaultValue : null);
  }

  public set(key: string, value: any): void {
    this.storage.set(key, value);
  }

  public remove(key: string): void {
    this.storage.remove(key);
  }

  public clear(): void {
    this.storage.clear();
  }
}
