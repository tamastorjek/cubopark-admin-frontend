import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ImageCompressorService {

  constructor() {
  }

  compress(file: File, maxSize = 1200): Observable<any> {
    const reader = new FileReader();
    let width;
    let height;
    let scaleFactor;

    reader.readAsDataURL(file);

    return new Observable(observer => {
      reader.onload = ev => {
        const img = new Image();
        img.src = (ev.target as any).result;

        (img.onload = () => {
          // Use Angular's Renderer2 method
          const elem = document.createElement('canvas');

          if (img.width > img.height) {
            scaleFactor = maxSize / img.width;
            width = maxSize;
            height = img.height * scaleFactor;
          } else {
            scaleFactor = maxSize / img.height;
            height = maxSize;
            width = img.width * scaleFactor;
          }

          elem.width = width;
          elem.height = height;

          const ctx = elem.getContext('2d') as CanvasRenderingContext2D;

          ctx.drawImage(img, 0, 0, width, height);

          ctx.canvas.toBlob(
            blob => {
              observer.next({
                file: new File([blob], file.name, {
                  type: 'image/jpeg',
                  lastModified: Date.now(),
                }),
                size: [width, height]
              });
            },
            'image/jpeg',
            0.8,
          );
        });
      };

      reader.onerror = error => observer.error(error);
    });
  }
}
