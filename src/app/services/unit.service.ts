import {Injectable} from '@angular/core';
import {ApiService} from './api.service';
import {Observable} from 'rxjs';
import {Unit} from '../models/unit';
import {OccupiedUnit} from '../models/occupied-unit';
import {JsonResponse} from '../models/json-response';

@Injectable({
  providedIn: 'root'
})
export class UnitService {
  private units: Observable<Unit[]>;
  private occupiedUnits: Observable<OccupiedUnit[]>;
  private freeUnits: Observable<Unit[]>;

  constructor(private api: ApiService) {
  }

  public loadUnits(quiet = false): Observable<JsonResponse> {
    return this.api.get('units');
  }

  public loadOccupiedUnits(): Observable<JsonResponse> {
    return this.api.get('units/occupied');
  }

  public loadFreeUnits(): Observable<JsonResponse> {
    return this.api.get('units/free');
  }
}
