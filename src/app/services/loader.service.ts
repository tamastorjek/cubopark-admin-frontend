import {Injectable} from '@angular/core';
import {LoadingController} from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class LoaderService {
  private instance;
  private visible = false;
  private loaders: string[] = [];

  constructor(private loader: LoadingController) {
  }

  async create() {
    this.instance = await this.loader.create({
      spinner: 'crescent',
      translucent: true,
      // message: text,
    });

    return await this.instance.present();
  }

  public show(loaderId: string) {
    this.loaders.push(loaderId);

    if (this.loaders.length > 1) {
      return;
    }

    this.create();
    this.visible = true;
  }

  public hide(loaderId: string) {
    const loaderIndex: number = this.loaders.indexOf(loaderId);

    if (loaderIndex === -1) {
      return;
    }

    this.loaders.splice(loaderIndex, 1);

    if (!this.loaders.length) {
      this.instance.dismiss();
      this.visible = false;
    }
  }
}
