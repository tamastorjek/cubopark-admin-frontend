import {Injectable, isDevMode} from '@angular/core';
import {ApiService} from './api.service';
import {NotificationService} from './notification.service';
import {Observable, Subject} from 'rxjs';
import {Payment} from '../models/payment';
import {JsonResponse} from 'src/app/models/json-response';

@Injectable({
  providedIn: 'root'
})
export class PaymentService {
  protected paymentSavedSubject = new Subject<Payment>();

  constructor(private api: ApiService, private notification: NotificationService) {
  }

  public get paymentSaved(): Observable<Payment> {
    return this.paymentSavedSubject.asObservable();
  }

  public store(payment: Payment): Observable<JsonResponse> {
    const formData = new FormData();

    for (const field of Object.keys(payment)) {
      switch (field) {
        case 'proof_file':
          if (payment[field] !== null) {
            formData.append(field, payment[field], payment[field].name);
          }
          break;

        case 'invoices_paid':
          formData.append(field, JSON.stringify(payment[field]));
          break;

        default:
          if (payment[field] !== null) {
            formData.append(field, payment[field]);
          }
          break;
      }
    }

    const response = this.api.post('payments', formData);

    response.subscribe((res) => {
      if (res.status === 'error') {
        if (isDevMode()) {
          console.log(res.messages);
        }
        return;
      }

      this.notification.toast('Payment saved.');
      this.paymentSavedSubject.next(res.data);
    });

    return response;
  }
}
