import {Injectable} from '@angular/core';
import {ApiService} from './api.service';
import {Tenant} from '../models/tenant';
import {Observable} from 'rxjs';
import {JsonResponse} from '../models/json-response';
import {select, Store} from '@ngrx/store';
import {getTenantById, getTenants} from '../store/selectors/tenant.selector';
import {Contact} from '../models/contact';
import {AppState} from '../store/state/app.state';
import {take} from 'rxjs/operators';
import {TenantActionTypes} from '../store/actions/tenant.actions';

@Injectable({
  providedIn: 'root'
})
export class TenantService {
  private loaded: number[] = [];
  private readonly tenants: Observable<Tenant[]>;

  constructor(
    private store: Store<AppState>,
    private api: ApiService
  ) {
    this.tenants = store.select(getTenants);
  }


  public loadTenants(quiet = false): Observable<JsonResponse> {
    this.loaded.push(0);
    return this.api.get('tenants', [], quiet ? {loadInBG: true} : {});
  }

  public loadTenant(id: number, quiet = false): Observable<JsonResponse> {
    this.loaded.push(id);
    return this.api.get('tenants', [id], quiet ? {loadInBG: true} : {});
  }

  public getTenants(): Observable<Tenant[]> {
    if (this.loaded.indexOf(0) === -1) {
      // load tenants
      this.tenants
        .pipe(take(1))
        .subscribe(tenants => {
          // if nothing was restored from storage, load with loader visible, otherwise in background
          this.store.dispatch({
            type: tenants.length > 0 ? TenantActionTypes.LOAD_LIST_QUIET : TenantActionTypes.LOAD_LIST
          });
        });
    }

    return this.tenants;
  }

  public getTenant(tenantId: number): Observable<Tenant> {
    return this.store.pipe(select(getTenantById, {id: tenantId}));
  }

  public saveTenant(tenantModel: Tenant): Observable<JsonResponse> {
    return this.api.put('tenants/' + tenantModel.id, {tenant: tenantModel, action: 'info'});
  }

  public saveContact(contactModel: Contact): Observable<JsonResponse> {
    return this.api.put('contacts/' + contactModel.id, contactModel);
  }

  public deleteTenantContact(contactModel: Contact): Observable<JsonResponse> {
    return this.api.delete('contacts/' + contactModel.id);
  }
}
