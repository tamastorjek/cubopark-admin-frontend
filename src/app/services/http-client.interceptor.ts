import {Injectable, isDevMode} from '@angular/core';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {ApiService} from './api.service';
import {Observable} from 'rxjs';

@Injectable()
export class HttpClientInterceptor implements HttpInterceptor {
  constructor(private api: ApiService) {
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    request = request.clone({
      setHeaders: {
        Authorization: 'Bearer ' + this.api.getToken(),
        IsDevMode: isDevMode() ? '1' : '0'
      }
    });

    return next.handle(request);
  }
}
