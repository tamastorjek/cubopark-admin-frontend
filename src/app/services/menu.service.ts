import {Injectable} from '@angular/core';
import {Observable, Subject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MenuService {

  private menuSubject: Subject<string> = new Subject();
  private toolbarTitleSubject = new Subject();
  private toolbarVisibleSubject = new Subject();
  private tabBarVisibleSubject = new Subject();

  get menuStateChange(): Observable<string> {
    return this.menuSubject.asObservable();
  }

  get toolbarTitleChange(): Observable<any> {
    return this.toolbarTitleSubject.asObservable();
  }

  get toolbarVisibleChange(): Observable<any> {
    return this.toolbarVisibleSubject.asObservable();
  }

  get tabbarVisibleChange(): Observable<any> {
    return this.tabBarVisibleSubject.asObservable();
  }

  set toolbarTitle(title: string) {
    this.toolbarTitleSubject.next(title);
  }

  set toolbarVisible(visible: boolean) {
    this.toolbarVisibleSubject.next(visible);
  }

  set tabBarVisible(visible: boolean) {
    this.tabBarVisibleSubject.next(visible);
  }

  constructor() {
  }

  open() {
    this.menuSubject.next('open');
  }

  close() {
    this.menuSubject.next('close');
  }
}
