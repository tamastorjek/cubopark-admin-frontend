import {Injector} from '@angular/core';
import {Router} from '@angular/router';

export class HelperService {
  public static injector: Injector;
  public static router: Router;

  public static getCurrentUrl(): string {
    return HelperService.router.url;
  }
}
