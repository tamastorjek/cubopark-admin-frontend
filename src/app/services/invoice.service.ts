import {Injectable} from '@angular/core';
import {ApiService} from './api.service';
import {Observable, Subject} from 'rxjs';
import {JsonResponse} from '../models/json-response';
import {Invoice} from '../models/invoice';
import {NotificationService} from './notification.service';

@Injectable({
  providedIn: 'root'
})
export class InvoiceService {
  private typeChangeSubject: Subject<string> = new Subject();

  get typeChange(): Observable<string> {
    return this.typeChangeSubject.asObservable();
  }

  private missingRentalInvoices: any[];
  private missingRentalInvoicesLoadedSubject: Subject<any> = new Subject();

  get missingRentalInvoicesLoaded(): Observable<any> {
    return this.missingRentalInvoicesLoadedSubject.asObservable();
  }

  private invoiceSavedSubject: Subject<boolean> = new Subject();

  get invoiceSaved(): Observable<boolean> {
    return this.invoiceSavedSubject.asObservable();
  }

  private lastMissingRentalInvoiceTenant: string = null;

  constructor(
    private api: ApiService,
    private notification: NotificationService
  ) {
  }

  public getByTenant(tenantId: number, paid = 'all', withPayments = false): Observable<JsonResponse> {
    return this.api.get(
      'invoices/' + tenantId + '/' + paid + (withPayments ? '/1' : '')
    );
  }

  public getTenantsWithInvoices(paid = 'all'): Observable<JsonResponse> {
    return this.api.get('tenants/with-invoice/' + paid);
  }

  public getMissingRentalInvoices(): void {
    this.api.get('invoices/missing/rental')
      .subscribe(res => {
        this.missingRentalInvoices = res.data;
        this.missingRentalInvoicesLoadedSubject.next();
      });
  }

  public changeType(val: string): void {
    this.typeChangeSubject.next(val);
  }

  public getMissingRentalInvoice(index: string = null) {
    if (index === null) {
      return this.missingRentalInvoices;
    }

    if (this.missingRentalInvoices[index] === undefined) {
      return [];
    }

    // set index so we can delete from it later
    this.lastMissingRentalInvoiceTenant = index;

    return this.missingRentalInvoices[index];
  }

  public deleteMissingRentalInvoice(index: string, value: string): boolean {
    if (this.missingRentalInvoices[index] === undefined) {
      return false;
    }

    const itemIndex = this.missingRentalInvoices[index].indexOf(value);

    if (itemIndex === -1) {
      return false;
    }

    const removed = this.missingRentalInvoices[index].splice(itemIndex, 1);

    return removed.length > 0;
  }

  public store(invoice: Invoice) {
    this.api.post('invoices', invoice)
      .subscribe(res => {
        if (res.status === 'error') {
          this.invoiceSavedSubject.next(false);
          this.notification.alert('Error saving invoice. Please try again.');
          console.log(res);
          return;
        }

        this.notification.toast('Invoice saved.');
        this.invoiceSavedSubject.next(true);
      });
  }

  public sendStatement(tenantId: number, invoiceType: string = null) {
    this.api.get('statement/email/' + tenantId + (invoiceType === null ? '' : '/' + invoiceType))
      .subscribe(res => {
        if (res.status === 'error') {
          this.notification.alert('Error sending statement. Please try again.');
          console.log(res);
          return;
        }

        const recipients = [];

        for (const r of res.data.email) {
          recipients.push(r.email);
        }

        this.notification.toast('Statement sent to:<br>' + recipients.join('<br>'));
      });
  }
}
