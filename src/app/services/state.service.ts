import {Injectable} from '@angular/core';
import {StateStorageService} from './state-storage.service';
import {NavigationEnd, Router, RouterEvent} from '@angular/router';
import {filter} from 'rxjs/operators';
import {AppState} from '../store/state/app.state';
import {Store} from '@ngrx/store';
import {PageActionTypes} from '../store/actions/page.actions';
import {getState} from '../store/selectors/page.selector';
import {PageState} from '../store/state/page.state';
import {Observable, Subject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class StateService {
  private pageLoadedSubject = new Subject<string>();
  pageLoaded$ = this.pageLoadedSubject.asObservable();

  private readonly state: Observable<PageState>;

  constructor(
    private store: Store<AppState>,
    private router: Router,
    private storageService: StateStorageService
  ) {
    // load saved page state
    this.state = this.store.select(getState);

    // save an empty page state on every navigation end
    router.events.pipe(
      filter((event: RouterEvent) => event instanceof NavigationEnd)
    ).subscribe(() => {
      // if no operation triggers state save on the new page, we don't have to always reset it...
      // this.saveState({});
      this.pageLoadedSubject.next(this.getUrl());
    });
  }

  getUrl(): string {
    return this.router.url;
  }

  getState(): Observable<PageState> {
    return this.state;
  }

  /**
   * Save page state
   *
   * @param newState The new state
   */
  saveState(newState: {}): void {
    this.store.dispatch({
      type: PageActionTypes.SET_STATE,
      state: {
        url: this.getUrl(),
        data: newState
      }
    });
  }
}
