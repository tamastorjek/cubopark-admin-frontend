import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ConfigService {

  static readonly token = '********';

  static readonly utilities = {
    multiplier: {
      E: 0.6,
      W: 1.3
    }
  };

  static readonly businessTypes = [
    {
      type: 'retail',
      value: 'R',
      name: 'Retail',
      icon: 'md-mall'
    },
    {
      type: 'food',
      value: 'F',
      name: 'Food & Beverage',
      icon: 'md-cutlery'
    },
    {
      type: 'service',
      value: 'S',
      name: 'Service',
      icon: 'md-ticket-star'
    }
  ];

  static readonly payment = {
    types: new Map([
      ['BT', 'Bank transfer'],
      ['CHQ', 'Cheque'],
      ['CD', 'Cash deposit'],
      ['C', 'Cash']
    ])
  };

  static readonly invoice = {
    types: new Map([
      ['RT', 'Rental'],
      ['FF', 'Facility fee'],
      ['PE', 'Penalty'],
      ['TA', 'Booking/Deposits'],
      ['CT', 'Custom'],
      ['ST', 'Storno']
    ]),
    itemTitles: {
      discount: 'Discount (from total)',
      RT: 'Rent for the period of '
    },
  };

  constructor() {
  }
}
