import {Injectable} from '@angular/core';
import {ApiService} from './api.service';
import {Observable} from 'rxjs';
import {JsonResponse} from '../models/json-response';
import {Store} from '@ngrx/store';
import {AppState} from '../store/state/app.state';
import {take} from 'rxjs/operators';
import {Meter} from '../models/meter';
import {getMeters} from '../store/selectors/meter.selector';
import {MeterActionTypes} from '../store/actions/meter.actions';

@Injectable({
  providedIn: 'root'
})
export class MeterService {
  private loaded: number[] = [];
  private readonly meters: Observable<Meter[]>;

  constructor(
    private store: Store<AppState>,
    private api: ApiService
  ) {
    this.meters = store.select(getMeters);
  }

  public loadMeters(quiet = false): Observable<JsonResponse> {
    this.loaded.push(0);
    return this.api.get('meters', [], quiet ? {loadInBG: true} : {});
  }

  public getMeters(): Observable<Meter[]> {
    // load meters if haven't been refreshed
    if (this.loaded.indexOf(0) === -1) {
      this.meters
        .pipe(take(1))
        .subscribe(meters => {
          // if nothing was restored from storage, load with loader visible, otherwise in background
          const action = meters.length > 0 ? MeterActionTypes.LOAD_LIST_QUIET : MeterActionTypes.LOAD_LIST;
          this.store.dispatch({type: action});
        });
    }

    return this.meters;
  }
}
