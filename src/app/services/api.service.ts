import {Injectable, isDevMode} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {JsonResponse} from '../models/json-response';
import {LoaderService} from './loader.service';
import {catchError, tap} from 'rxjs/operators';
import {ConfigService} from './config.service';
import {NotificationService} from './notification.service';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  private readonly token;
  private apiUrl: string;
  private requestOptions = {responseType: 'json', observe: 'body'};

  constructor(
    private http: HttpClient,
    private loader: LoaderService,
    private notification: NotificationService
  ) {
    this.token = ConfigService.token;
    this.apiUrl = isDevMode() ? 'http://cpadmin.test/api/' : 'https://a.cubopark.com/api/';
  }

  private static logError(response: JsonResponse): void {
    if (response.status === 'error' && isDevMode()) {
      console.log(response);
    }
  }

  public getToken(): string {
    return this.token;
  }

  private processOptions(options): object {
    if (!options.loadInBG) {
      this.loader.show('api-service');
    }

    return {...options, ...this.requestOptions};
  }

  private handleError(error: any): Observable<never> {
    this.loader.hide('api-service');

    if (error.error.status === undefined) {
      const errorText = '[' + error.status + '] ' + error.message;

      this.notification.alert(errorText + '<br>Please try again!');
      console.log(errorText);
      return throwError(errorText);
    }

    if (error.error.status !== 'error' || error.error.messages === undefined) {
      this.notification.alert('Unknown error.<br>Please try again!');
      console.log('Unknown error:', error);
      return throwError('Unknown error');
    }

    for (const index of Object.keys(error.error.messages)) {
      console.log(index, error.error.messages[index], Array.isArray(error.error.messages[index]));

      if (!Array.isArray(error.error.messages[index])) {
        this.notification.alert(error.error.messages[index]);
        return throwError(error.error.messages[index]);
      }

      for (const subMessage of error.error.messages[index]) {
        this.notification.toast(subMessage);
      }
    }

    return throwError(error.message);
  }

  private showNotification(response: JsonResponse): void {
    if (!response.messages) {
      return;
    }

    for (const key of Object.keys(response.messages)) {
      this.notification.toast(response.messages[key]);
    }
  }

  private requestEnd(response: JsonResponse): void {
    ApiService.logError(response);
    this.showNotification(response);
    this.loader.hide('api-service');
  }

  public get(endpoint: string, data = [], options = {}): Observable<JsonResponse> {
    options = this.processOptions(options);

    return this.http.get<JsonResponse>(
      this.apiUrl + endpoint + (data.length ? '/' + data.join('/') : ''),
      options
    ).pipe(
      catchError(this.handleError.bind(this)),
      tap(this.requestEnd.bind(this))
    );
  }

  public post(endpoint: string, data: any, options = {}): Observable<JsonResponse> {
    options = this.processOptions(options);

    return this.http.post<JsonResponse>(
      this.apiUrl + endpoint,
      data,
      options
    ).pipe(
      catchError(this.handleError.bind(this)),
      tap(this.requestEnd.bind(this))
    );
  }

  public put(endpoint: string, data: any, options = {}): Observable<JsonResponse> {
    options = this.processOptions(options);

    return this.http.put<JsonResponse>(
      this.apiUrl + endpoint,
      data,
      options
    ).pipe(
      catchError(this.handleError.bind(this)),
      tap(this.requestEnd.bind(this))
    );
  }

  public delete(endpoint: string, data = [], options = {}): Observable<JsonResponse> {
    options = this.processOptions(options);

    return this.http.delete<JsonResponse>(
      this.apiUrl + endpoint + (data.length ? '/' + data.join('/') : ''),
      options
    ).pipe(
      catchError(this.handleError.bind(this)),
      tap(this.requestEnd.bind(this))
    );
  }
}
