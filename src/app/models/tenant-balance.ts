export class TenantBalance {
  invoiced: number;
  paid: number;
  current: number;

  constructor(invoiced = 0, paid = 0) {
    this.invoiced = invoiced;
    this.paid = paid;
    this.current = this.invoiced - this.paid;
  }
}
