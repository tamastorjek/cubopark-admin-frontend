export class FileUpload {
  file: File;
  extension: string;
  size: {x: number, y: number};
  url: string;
}
