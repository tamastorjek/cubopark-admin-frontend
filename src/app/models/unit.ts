import {Meter} from './meter';
import {Tenant} from './tenant';

export class Unit {
  id: number;
  block: string;
  number: string;
  area: number;
  type?: string|null;
  price: number;
  string: string;
  meter?: Meter;
  tenants?: Tenant[];
}
