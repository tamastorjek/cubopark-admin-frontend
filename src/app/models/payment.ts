import {Invoice} from './invoice';
import {dayjs} from 'src/app/helpers/date-helper';
import {toFloat, toInt} from '../helpers/number-formatter';

export class Payment {
  id: number;
  amount: number = null;
  type: string = null;
  reference: string = null;
  proof_file?: File;
  proof_file_ext: string = null;
  notes: string = null;
  year: number;
  month: number;
  created_at: string = dayjs().format('YYYY-MM-DD HH:mm:ss');
  amount_string: string;
  number: string;
  file: string;
  date: string;
  pdf_file: string;
  invoice_numbers?: Array<string>;
  invoices?: Array<Invoice>;
  invoices_paid?: Array<object>;

  // show amount breakdown for each related invoice
  amountBreakdown?: any;

  constructor(data = {}) {
    for (const prop of Object.keys(data)) {
      this[prop] = data[prop];
    }
  }

  get cdate(): string {
    return this.created_at.split(' ')[0];
  }

  set cdate(val) {
    this.created_at = (val === null ? dayjs().format('YYYY-MM-DD') : val) + ' 18:00:00';
  }

  get floatAmount() {
    return this.amount === null ? null : toFloat(this.amount);
  }

  set floatAmount(val) {
    this.amount = toInt(val);
  }
}
