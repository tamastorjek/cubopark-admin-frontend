export class Meter {
  id: string;
  unit_id: number;
  type: string;
  last_reading: number;
  updated_at: string;
  tenant_id?: number;

  disabled?: boolean;
}
