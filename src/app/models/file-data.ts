export class FileData {
  file: File;
  extension: string;
  size: { x: number, y: number };
  url: string;

  constructor(data: any = {}) {
    for (const prop of Object.keys(data)) {
      this[prop] = data[prop];
    }
  }

  isWider(): boolean {
    return this.size.y / this.size.x <= 0.75;
  }
}
