import {Tenant} from './tenant';

export class Contact {
  id: number;
  tenant_id: number;
  name: string;
  phone: string;
  email: string;
  primary: boolean;
  notes: string;
  tenant?: Tenant;
}
