export class JsonResponse {
  status?: string;
  data?: any;
  messages?: object;
}
