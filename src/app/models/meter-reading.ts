import {Tenant} from './tenant';
import {Meter} from './meter';
import {dayjs} from '../helpers/date-helper';
import {ReadingCalculation} from '../pages/meter-reading/meter-reading-form/reading-calculation.model';

export class MeterReading {
  tenant_id: number;
  meter_id: string;
  reading: number;
  type: string;
  proof_file: File;
  notes?: string;
  created_at?: string = dayjs().format('YYYY-MM-DD HH:mm:ss');
  file: string;
  tenant?: Tenant;
  meter?: Meter;
  calculations: ReadingCalculation = new ReadingCalculation();

  get date(): string {
    return this.created_at.split(' ')[0];
  }

  set date(val: string) {
    this.created_at = val + ' 18:00:00';
  }

  get readingValue(): number {
    return this.reading;
  }

  set readingValue(val) {
    this.reading = (typeof val === 'string') ? parseFloat(val) : val;
  }

  constructor(data: any = {}) {
    for (const prop of Object.keys(data)) {
      this[prop] = data[prop];
    }
  }

  update(meter: Meter = null): void {
    if (meter) {
      this.meter = meter;
      this.tenant_id = meter.tenant_id;
      this.meter_id = meter.id;
      this.type = meter.type;
    }

    this.calculations = new ReadingCalculation(this.meter);
    this.calculations.update(this.reading);
  }

  save(): MeterReading {
    const reading = new MeterReading();

    for (const prop of Object.keys(this)) {
      reading[prop] = typeof this[prop] === 'object' ? {...this[prop]} : this[prop];
    }

    return reading;
  }

  validate(): boolean {
    return this.tenant_id
      && this.meter_id
      && this.type
      && this.meter.last_reading < this.reading;
  }
}
