import {Tenant} from './tenant';

export class Document {
  id: number;
  tenant_id: number;
  type: number;
  title: string;
  extension: string;
  notes: string;
  tenant?: Tenant;
}
