import {toFloat, toInt} from '../helpers/number-formatter';

export class InvoiceItem {
  title: string = null;
  amount: number = null;
  comment: string = null;
  amount_string?: string;

  constructor(data = {}) {
    for (const prop of Object.keys(data)) {
      this[prop] = data[prop];
    }
  }

  get floatAmount() {
    return this.amount === null ? null : toFloat(this.amount);
  }

  set floatAmount(val) {
    this.amount = toInt(val);
  }
}
