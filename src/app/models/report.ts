import {Tenant} from './tenant';

export class Report {
  id: number;
  tenant_id: number;
  type: string;
  created_at: string;
  tenant?: Tenant;
}
