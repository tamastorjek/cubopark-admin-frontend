import {Unit} from './unit';
import {TenantBalance} from './tenant-balance';
import {Contact} from './contact';
import {Document} from './document';
import {Photo} from './photo';
import {Report} from './report';
import {Payment} from './payment';
import {toFloat, toInt} from '../helpers/number-formatter';
import {dayjs} from 'src/app/helpers/date-helper';
import {TenantFlags} from './tenant-flags';

export class Tenant {
  id: number;
  name = null;
  reg_number = null;
  address = null;
  shop_name = null;
  rental = null;
  balance = new TenantBalance();
  description = null;
  business_type = 'R';
  tenancy_length = 1;
  tenancy_start = dayjs().format('YYYY-MM-DD');
  notes = null;
  units_string: string;
  unit_ids: Array<number>;
  payments?: Array<Payment>;
  units?: Array<Unit>;
  contacts?: Array<Contact>;
  documents?: Array<Document>;
  photos?: Array<Photo>;
  reports?: Array<Report>;

  // flags
  flags: TenantFlags;

  constructor(data = {}) {
    for (const prop of Object.keys(data)) {
      this[prop] = data[prop];
    }
  }

  get rent() {
    return this.rental === null ? null : toFloat(this.rental);
  }

  set rent(val) {
    this.rental = toInt(val);
  }
}
