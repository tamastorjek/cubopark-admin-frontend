import {dayjs} from 'src/app/helpers/date-helper';
import {toFloat, toInt} from '../helpers/number-formatter';

import {Tenant} from './tenant';
import {InvoiceItem} from './invoice-item';
import {Payment} from './payment';

export class Invoice {
  id: number;
  tenant_id: number;
  type: string = null;
  amount: number = null;
  items: Array<InvoiceItem> = [];
  paid = false;
  paid_amount = 0;
  year: number;
  month: number;
  created_at: string = dayjs().format('YYYY-MM-DD HH:mm:ss');
  amount_string: string;
  paid_amount_string: string;
  date: string;
  due: string;
  number: string;
  file: string;
  balance: number = null;
  balance_string?: string;
  tenant?: Tenant;
  payments?: Array<Payment>;

  constructor(data = {}) {
    for (const prop of Object.keys(data)) {
      this[prop] = data[prop];
    }
  }

  get cdate(): string {
    return this.created_at.split(' ')[0];
  }

  set cdate(val) {
    this.created_at = (val === null ? dayjs().format('YYYY-MM-DD') : val) + ' 18:00:00';
  }

  get floatAmount(): number {
    return this.amount === null ? null : toFloat(this.amount);
  }

  set floatAmount(val) {
    this.amount = toInt(val);
  }

  get floatPaidAmount(): number {
    return this.paid_amount === null ? null : toFloat(this.paid_amount);
  }

  set floatPaidAmount(val) {
    this.paid_amount = toInt(val);
  }

  get floatBalance(): number {
    return this.balance === null ? null : toFloat(this.balance);
  }

  set floatBalance(val) {
    this.balance = toInt(val);
  }
}
