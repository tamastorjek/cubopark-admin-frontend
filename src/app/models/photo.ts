import {Tenant} from './tenant';

export class Photo {
  id: number;
  tenant_id: number;
  description: string;
  tenant?: Tenant;
}
