import {Meter} from './meter';

export class OccupiedUnit {
  tenant_id: number;
  id: Array<number>;
  block: string;
  numbers: Array<string>;
  string: string;
  meters: Array<Meter>;
  disabled: boolean;
  index: number;
  label: string;
}
