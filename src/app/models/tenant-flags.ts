export class TenantFlags {
  no_tenancy_start?: boolean;
  missing_business_info?: boolean;
  rent_invoices: number;
  months: number;
  no_email?: boolean;
}
