import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {IonicModule} from '@ionic/angular';
import {RouterModule} from '@angular/router';

import {HomePage} from './home.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild([
      {
        path: '',
        redirectTo: 'home',
        pathMatch: 'full'
      },
      {
        path: '',
        component: HomePage,
        children: [
          {
            path: 'home',
            redirectTo: 'dashboard',
            pathMatch: 'full'
          },
          {
            path: 'dashboard',
            loadChildren: () => import('../dashboard/dashboard.module')
              .then(m => m.DashboardPageModule)
          },
          {
            path: 'tenant-list',
            loadChildren: () => import('../tenant/tenant-list/tenant-list.module')
              .then(m => m.TenantListPageModule)
          },
          {
            path: 'invoice-form',
            loadChildren: () => import('../invoice/invoice-form/invoice-form.module')
              .then(m => m.InvoiceFormPageModule)
          },
          {
            path: 'payment-form',
            loadChildren: () => import('../payment/payment-form/payment-form.module')
              .then(m => m.PaymentFormPageModule)
          },
          {
            path: 'meter-reading-form',
            loadChildren: () => import('../meter-reading/meter-reading-form/meter-reading-form.module')
              .then(m => m.MeterReadingFormPageModule)
          }
        ]
      }
    ])
  ],
  declarations: [HomePage]
})
export class HomePageModule {
}
