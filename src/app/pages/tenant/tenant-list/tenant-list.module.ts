import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {RouterModule, Routes} from '@angular/router';

import {IonicModule} from '@ionic/angular';

import {TenantListPage} from './tenant-list.page';
import {TenantService} from '../../../services/tenant.service';
import {FloatPipe} from '../../../pipes/float.pipe';
import {SearchbarComponentModule} from '../../../components/shared/searchbar/searchbar.module';

const routes: Routes = [
  {
    path: '',
    component: TenantListPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    SearchbarComponentModule
  ],
  declarations: [TenantListPage, FloatPipe]
})
export class TenantListPageModule {
}
