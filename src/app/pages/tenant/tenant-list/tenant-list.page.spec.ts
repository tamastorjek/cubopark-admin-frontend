import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TenantListPage } from './tenant-list.page';

describe('TenantListPage', () => {
  let component: TenantListPage;
  let fixture: ComponentFixture<TenantListPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TenantListPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TenantListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
