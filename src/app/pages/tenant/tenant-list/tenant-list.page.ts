import {Component, OnInit} from '@angular/core';
import {Tenant} from '../../../models/tenant';
import {Observable} from 'rxjs';
import {TenantService} from '../../../services/tenant.service';
import {SearchbarResult} from '../../../components/shared/searchbar/searchbar-result.model';
import {StateService} from '../../../services/state.service';

@Component({
  selector: 'app-tenant-list',
  templateUrl: './tenant-list.page.html',
  styleUrls: ['./tenant-list.page.scss'],
})
export class TenantListPage implements OnInit {
  tenants: Observable<Tenant[]> = this.tenantService.getTenants();
  searchResults: SearchbarResult = new SearchbarResult();

  constructor(
    private stateService: StateService,
    private tenantService: TenantService
  ) {
  }

  ngOnInit() {
  }

  /**
   * Save results and save search term in state
   *
   * @param results Search results
   */
  onResultChange(results: SearchbarResult): void {
    this.searchResults = results;
  }

  /**
   * Check if the given tenant ID is in the search results
   *
   * @param id Tenant ID
   */
  inSearchResults(id: number): boolean {
    if (this.searchResults.results === null) {
      return true;
    }

    return this.searchResults.results.find(tenantId => tenantId === id) !== undefined;
  }
}
