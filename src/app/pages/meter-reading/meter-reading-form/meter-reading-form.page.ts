import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {Observable} from 'rxjs';
import {Tenant} from '../../../models/tenant';
import {AppState} from '../../../store/state/app.state';
import {Store} from '@ngrx/store';
import {TenantService} from '../../../services/tenant.service';
import {ModalSearchResult} from '../../../components/shared/modal-search/modal-search-result.model';
import {MeterService} from '../../../services/meter.service';
import {Meter} from '../../../models/meter';
import {StateService} from '../../../services/state.service';
import {MeterReading} from '../../../models/meter-reading';
import {LoadState} from '../../../decorators/load-state.decorator';
import {take} from 'rxjs/operators';
import {dayjs} from '../../../helpers/date-helper';
import {find} from 'lodash-es';
import {NotificationService} from '../../../services/notification.service';

@Component({
  selector: 'app-meter-reading-form',
  templateUrl: './meter-reading-form.page.html',
  styleUrls: ['./meter-reading-form.page.scss'],
})
@LoadState()
export class MeterReadingFormPage implements OnInit {

  constructor(
    private store: Store<AppState>,
    private stateService: StateService,
    private meterService: MeterService,
    private tenantService: TenantService,
    private changeDetector: ChangeDetectorRef,
    private notification: NotificationService
  ) {
  }

  get checkedMeter(): string {
    if (!this.meterReading || this.meterReading && !this.meterReading.meter) {
      return null;
    }

    return this.meterReading.meter.id;
  }

  set checkedMeter(meterId: string) {
    // do nothing if new=current
    if (this.meterReading.meter && meterId === this.meterReading.meter.id) {
      return;
    }

    // stop if there's no selection
    if (!meterId) {
      return;
    }

    this.meterReading.update(find(this.tenantMeters, (m: Meter) => m.id === meterId));
    this.saveState();
  }

  meters: Observable<Meter[]> = this.meterService.getMeters();
  tenants: Observable<Tenant[]> = this.tenantService.getTenants();
  selection: ModalSearchResult = new ModalSearchResult();
  tenantMeters: Meter[] = [];
  meterReading: MeterReading = new MeterReading();

  private static getTenantMeters(tenantId: number, meters: Meter[]): Meter[] {
    // 2 weeks ago
    const validDate = dayjs().subtract(2, 'week');
    const tenantMeters: Meter[] = [];

    for (let meter of meters) {
      // if the meter doesn't belong to the tenant, ignore it
      if (meter.tenant_id !== tenantId) {
        continue;
      }

      // HACK
      if (!Object.isExtensible(meter)) {
        meter = {...meter};
      }

      // disable meter
      meter.disabled = !dayjs(meter.updated_at).isBefore(validDate);

      tenantMeters.push(meter);
    }

    return tenantMeters;
  }

  ngOnInit() {
  }

  private loadState(data: any): void {
    if (!Object.keys(data).length) {
      return;
    }

    this.meterReading = new MeterReading(data.reading);
    this.meterReading.update();
    this.tenantMeters = data.tenantMeters;
    this.selection = data.selection;
    this.changeDetector.detectChanges();
  }

  private saveState(): void {
    this.stateService.saveState({
      selection: {...this.selection},
      reading: this.meterReading.save(),
      tenantMeters: [...this.tenantMeters]
    });
  }

  onSelectionChange(newSelection: ModalSearchResult): void {
    if (this.selection.key === newSelection.key) {
      return;
    }

    this.meters
      .pipe(take(1))
      .subscribe(
        meters => this.setForm(newSelection, meters)
      );
  }

  private resetForm(): void {
    this.selection = new ModalSearchResult();
    this.tenantMeters = [];
    this.meterReading = new MeterReading();
    // force detect changes
    this.changeDetector.detectChanges();
  }

  private setForm(newSelection: ModalSearchResult, meters: Meter[]): void {
    // reset everything first
    this.resetForm();
    // get tenant meters
    const tenantMeters: Meter[] = MeterReadingFormPage.getTenantMeters(newSelection.key, meters);
    // get checked meter
    const checkedMeter: Meter = find(tenantMeters, (m: Meter) => !m.disabled);

    if (!checkedMeter) {
      console.log('No more meter!');
    }

    // set up meter reading with current meter
    this.meterReading.update(checkedMeter);
    this.tenantMeters = tenantMeters;
    this.selection = newSelection;
    // force detect changes
    this.changeDetector.detectChanges();
    this.saveState();
  }

  setReadingProperty(prop: string, val: any, saveState = true) {
    this.meterReading[prop] = val;

    if (prop === 'readingValue') {
      this.meterReading.calculations.update(this.meterReading.readingValue);
    }

    if (saveState) {
      this.saveState();
    }
  }

  updateProofFile(file: File): void {
    this.meterReading.proof_file = file;
  }

  save(confirmed = false): void {
    if (!this.meterReading.proof_file && !confirmed) {
      this.notification.confirm('Save without proof image?', () => this.save(true));

      return;
    }

    console.log('Save confirmed! Validate: ' + this.meterReading.validate());
  }
}
