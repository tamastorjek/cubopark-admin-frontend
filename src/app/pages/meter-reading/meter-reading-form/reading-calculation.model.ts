import {ConfigService} from '../../../services/config.service';
import {Meter} from '../../../models/meter';

export class ReadingCalculation {
  last = 0;
  usage = 0;
  amount = 0;

  private readonly type: string;
  private readonly multiplier: number;

  constructor(meter: Meter = null) {
    if (!meter) {
      this.type = null;
      this.multiplier = 1;
      return;
    }

    this.last = meter.last_reading;
    this.type = meter.type;
    this.multiplier = ConfigService.utilities.multiplier[meter.type];
  }

  update(reading: number): void {
    if (!reading || reading && reading < this.last) {
      this.usage = 0;
      this.amount = 0;
      return;
    }

    this.usage = Math.round((reading - this.last) * 10) / 10;
    this.amount = Math.ceil(this.multiplier * this.usage * 100);
  }
}
