import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {RouterModule, Routes} from '@angular/router';
import {IonicModule} from '@ionic/angular';
import {MeterReadingFormPage} from './meter-reading-form.page';
import {ModalSearchbarComponentModule} from '../../../components/shared/modal-searchbar/modal-searchbar.module';
import {MeterService} from '../../../services/meter.service';
import {EffectsModule} from '@ngrx/effects';
import {MeterEffects} from '../../../store/effects/meter.effects';
import {FloatCurrencyPipe} from '../../../pipes/float-currency.pipe';
import {ImageUploaderModule} from '../../../components/shared/image-uploader/image-uploader.module';

const routes: Routes = [
  {
    path: '',
    component: MeterReadingFormPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    EffectsModule.forFeature([MeterEffects]),
    ModalSearchbarComponentModule,
    ImageUploaderModule
  ],
  providers: [
    MeterService
  ],
  declarations: [MeterReadingFormPage, FloatCurrencyPipe]
})
export class MeterReadingFormPageModule {
}
