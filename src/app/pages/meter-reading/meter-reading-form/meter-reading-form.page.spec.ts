import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MeterReadingFormPage } from './meter-reading-form.page';

describe('MeterReadingFormPage', () => {
  let component: MeterReadingFormPage;
  let fixture: ComponentFixture<MeterReadingFormPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MeterReadingFormPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MeterReadingFormPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
