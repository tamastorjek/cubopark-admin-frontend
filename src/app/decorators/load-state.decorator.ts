import {getState} from '../store/selectors/page.selector';
import {take} from 'rxjs/operators';
import {PageState} from '../store/state/page.state';

export function LoadState(): ClassDecorator {
  return (constructor: any) => {
    // save original method
    const original = constructor.prototype.ngOnInit;

    if (typeof constructor.prototype.ngOnInit !== 'function') {
      throw new Error('Must implement OnInit!');
    }

    if (typeof constructor.prototype.loadState !== 'function') {
      throw new Error('Method loadState cannot be found!');
    }

    // override ngOnInit
    constructor.prototype.ngOnInit = function(...args) {
      if (typeof original === 'function') {
        // execute original method
        original.apply(this, arguments);
      }

      // subscribe to page state
      this.stateService.getState()
        .pipe(take(1))
        .subscribe((state: PageState) => {
          this.loadState(state.url === this.stateService.getUrl() ? state.data : {});
        });
    };
  };
}
