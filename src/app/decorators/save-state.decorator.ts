export function SaveState(
  {
    method = 'ngOnInit',
    prop = 'pageUrl'
  } = {}): ClassDecorator {
  return (constructor: any) => {
    // save original method
    const original = constructor.prototype[method];

    // override method
    constructor.prototype[method] = function(...args) {
      if (typeof original === 'function') {
        // execute original method
        original.apply(this, arguments);
      }

      // set page url
      this[prop] = this.stateService.getUrl();

      this.stateService.pageLoaded$.subscribe(url => url === this[prop] && this.saveState());

      console.log(this[prop]);
    };
  };
}
