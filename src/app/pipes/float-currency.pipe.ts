import {Pipe, PipeTransform} from '@angular/core';
import {CurrencyPipe} from '@angular/common';
import {toFloat} from '../helpers/number-formatter';

@Pipe({
  name: 'floatCurrency'
})
export class FloatCurrencyPipe extends CurrencyPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    return super.transform(toFloat(value), args);
  }

}
