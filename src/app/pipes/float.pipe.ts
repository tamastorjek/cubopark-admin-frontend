import { Pipe, PipeTransform } from '@angular/core';
import {toFloat} from '../helpers/number-formatter';
import {DecimalPipe} from '@angular/common';

@Pipe({
  name: 'float'
})
export class FloatPipe extends DecimalPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    return super.transform(toFloat(value), args);
  }

}
