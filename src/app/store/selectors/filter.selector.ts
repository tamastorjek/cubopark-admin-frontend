import {createSelector} from '@ngrx/store';
import {AppState} from '../state/app.state';
import {FilterState} from '../state/filter.state';

export const getFilterFromState = (state: AppState) => state.filter;

export const getTenantListSearchTerm = createSelector(
  getFilterFromState,
  (state: FilterState) => state.tenantList.searchTerm
);

export const getMeterReadingFormSelection = createSelector(
  getFilterFromState,
  (state: FilterState) => state.meterReadingForm
);
