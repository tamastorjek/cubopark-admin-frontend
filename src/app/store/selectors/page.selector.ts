import {createSelector} from '@ngrx/store';
import {AppState} from '../state/app.state';
import {PageState} from '../state/page.state';

export const getPageFromState = (state: AppState) => state.page;

export const getState = createSelector(
  getPageFromState,
  (state: PageState) => state
);

export const getUrl = createSelector(
  getPageFromState,
  (state: PageState) => state.url
);

export const getData = createSelector(
  getPageFromState,
  (state: PageState) => state.data
);
