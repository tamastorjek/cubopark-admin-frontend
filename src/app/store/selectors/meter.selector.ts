import {createSelector} from '@ngrx/store';
import {AppState} from '../state/app.state';
import {MeterState} from '../state/meter.state';

export const getMetersFromState = (state: AppState) => state.meter;

export const getMeters = createSelector(
  getMetersFromState,
  (state: MeterState) => state.list
);

export const getMeterByTenantId = createSelector(
  getMetersFromState,
  (state: MeterState, props: { tenantId: number }) => {
    return state.list.find(meter => meter.tenant_id === props.tenantId);
  }
);
