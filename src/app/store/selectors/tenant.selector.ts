import {createSelector} from '@ngrx/store';
import {TenantState} from '../state/tenant.state';
import {AppState} from '../state/app.state';

export const getTenantsFromState = (state: AppState) => state.tenant;

export const getTenants = createSelector(
  getTenantsFromState,
  (state: TenantState) => state.list
);

export const getTenantById = createSelector(
  getTenantsFromState,
  (state: TenantState, props: { id: number }) => {
    return state.list.find(tenant => tenant.id === props.id);
  }
);

export const getSelectedTenant = createSelector(
  getTenantsFromState,
  (state: TenantState) => state.selected
);
