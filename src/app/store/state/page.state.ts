export interface PageState {
  url: string;
  data: {};
}

export const initialPageState: PageState = {
  url: '/home/dashboard',
  data: {}
};
