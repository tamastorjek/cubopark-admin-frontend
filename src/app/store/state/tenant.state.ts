import {Tenant} from '../../models/tenant';

export interface TenantState {
  list: Tenant[];
  details: Tenant[];
  selected: Tenant;
}

export const initialTenantState: TenantState = {
  list: [],
  details: [],
  selected: null
};
