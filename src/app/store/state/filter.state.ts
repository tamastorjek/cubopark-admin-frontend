import {ModalSearchResult} from '../../components/shared/modal-search/modal-search-result.model';

export interface FilterState {
  tenantList: any;
  meterReadingForm: ModalSearchResult;
}

export const initialFilterState: FilterState = {
  tenantList: {
    searchTerm: ''
  },
  meterReadingForm: new ModalSearchResult()
};
