import * as fromRouter from '@ngrx/router-store';
import {TenantState} from './tenant.state';
import {FilterState} from './filter.state';
import {MeterState} from './meter.state';
import {PageState} from './page.state';

export interface AppState {
  router: fromRouter.RouterReducerState<any>;
  filter: FilterState;
  tenant: TenantState;
  meter: MeterState;
  page: PageState;
}
