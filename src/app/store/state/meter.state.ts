import {Meter} from '../../models/meter';

export interface MeterState {
  list: Meter[];
}

export const initialMeterState: MeterState = {
  list: []
};
