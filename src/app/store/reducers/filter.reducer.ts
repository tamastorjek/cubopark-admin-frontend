import {Action, createReducer, on} from '@ngrx/store';
import * as FilterActions from '../actions/filter.actions';
import {FilterState, initialFilterState} from '../state/filter.state';
import {merge} from 'lodash-es';

const filterReducer = createReducer(
  initialFilterState,
  // Tenant list search term
  on(
    FilterActions.setTenantListSearchTerm,
    (state, {term}) => {
      const replace = {tenantList: {searchTerm: term}};

      return {...state, ...replace};
    }
  ),
  // meter reading form selection
  on(
    FilterActions.setMeterReadingFormSelection,
    (state, {selection}) => ({...state, meterReadingForm: selection})
  )
);

export function reducer(state: FilterState | undefined, action: Action) {
  return filterReducer(state, action);
}
