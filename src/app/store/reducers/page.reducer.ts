import {Action, createReducer, on} from '@ngrx/store';
import * as PageActions from '../actions/page.actions';
import {initialPageState, PageState} from '../state/page.state';

const pageReducer = createReducer(
  initialPageState,
  // set full state
  on(PageActions.setPageState, (oldState, {state}) => state),
  on(PageActions.setPageData, (state, {pageData}) => ({...state, data: pageData}))
);

export function reducer(state: PageState | undefined, action: Action) {
  return pageReducer(state, action);
}
