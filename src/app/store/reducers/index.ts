import {ActionReducerMap, createFeatureSelector} from '@ngrx/store';
import * as fromRouter from '@ngrx/router-store';
import * as fromFilter from './filter.reducer';
import * as fromTenant from './tenant.reducer';
import * as fromMeter from './meter.reducer';
import * as fromPage from './page.reducer';
import {AppState} from '../state/app.state';

export const selectRouter = createFeatureSelector<AppState, fromRouter.RouterReducerState<any>>('router');

const {
  selectQueryParams,    // select the current route query params
  selectQueryParam,     // factory function to select a query param
  selectRouteParams,    // select the current route params
  selectRouteParam,     // factory function to select a route param
  selectRouteData,      // select the current route data
  selectUrl,            // select the current url
} = fromRouter.getSelectors(selectRouter);

export const reducers: ActionReducerMap<AppState> = {
  router: fromRouter.routerReducer,
  filter: fromFilter.reducer,
  tenant: fromTenant.reducer,
  meter: fromMeter.reducer,
  page: fromPage.reducer
};
