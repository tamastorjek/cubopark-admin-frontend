import {Action, createReducer, on} from '@ngrx/store';
import * as TenantActions from '../actions/tenant.actions';
import {initialTenantState, TenantState} from '../state/tenant.state';

const tenantReducer = createReducer(
  initialTenantState,
  on(TenantActions.tenantListLoaded, (state, {result}) => ({...state, list: result})),
  on(TenantActions.tenantLoaded, (state, {result}) => {
    const nextState = {...state};

    nextState.details[result.id] = result;

    return nextState;
  })
);

export function reducer(state: TenantState | undefined, action: Action) {
  return tenantReducer(state, action);
}
