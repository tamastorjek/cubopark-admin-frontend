import {Action, createReducer, on} from '@ngrx/store';
import * as MeterActions from '../actions/meter.actions';
import {initialMeterState, MeterState} from '../state/meter.state';

const meterReducer = createReducer(
  initialMeterState,
  // meter list
  on(MeterActions.meterListLoaded, (state, {result}) => ({...state, list: result}))
);

export function reducer(state: MeterState | undefined, action: Action) {
  return meterReducer(state, action);
}
