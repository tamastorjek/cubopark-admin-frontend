import {Action, ActionReducer, MetaReducer} from '@ngrx/store';
import {AppState} from '../state/app.state';
import {has, merge, pick} from 'lodash-es';
import {PageState} from '../state/page.state';

const stateKeys = [
  'page', // THIS MUST BE FIRST!!!
  'filter',
  'tenant',
  'meter',
  // 'router.state.url'
];

export function saveState(reducer: ActionReducer<any>): ActionReducer<any> {
  let stateLoaded = false;

  return (state: AppState, action: Action) => {
    // console.log('META Action: ' + action.type);
    let nextState = reducer(state, action);

    if (!stateLoaded) {
      for (const key of stateKeys) {
        if (!has(nextState, key)) {
          // console.log('META No Key: ' + key);
          continue;
        }

        const savedValue = JSON.parse(localStorage.getItem('state_' + key));

        if (savedValue === null) {
          continue;
        }

        // make sure we load the last visited page
        if (key === 'page') {
          // get last page
          const lastUrl = savedValue.page.url;
          const loc: Location = window.location;

          // redirect to last page
          if (loc.pathname !== lastUrl) {
            loc.replace(loc.protocol + '//' + loc.host + lastUrl);
            return nextState;
          }
        }

        nextState = merge(nextState, savedValue);
      }

      stateLoaded = true;
      return nextState;
    }

    for (const key of stateKeys) {
      // console.log('LS Setting: ' + key);
      localStorage.setItem('state_' + key, JSON.stringify(pick(nextState, key)));
    }

    return nextState;
  };
}

export const metaReducers: MetaReducer<any>[] = [saveState];
