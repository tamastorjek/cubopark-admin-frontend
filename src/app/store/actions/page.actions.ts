import {createAction, props} from '@ngrx/store';
import {PageState} from '../state/page.state';

export enum PageActionTypes {
  SET_STATE = '[Page] Set Full State',
  SET_DATA = '[Page] Set Data'
}

export const setPageState = createAction(
  PageActionTypes.SET_STATE,
  props<{ state: PageState }>()
);

export const setPageData = createAction(
  PageActionTypes.SET_DATA,
  props<{ pageData: {} }>()
);
