import {createAction, props} from '@ngrx/store';
import {Tenant} from '../../models/tenant';

export enum TenantActionTypes {
  LIST_LOADED = '[Tenant] Tenant List Loaded',
  LOAD_LIST = '[Tenant] Load Tenant List',
  LOAD_LIST_QUIET = '[Tenant] Load Tenant List Quietly',
  LOAD = '[Tenant] Load',
  LOAD_QUIET = '[Tenant] Load Quietly',
  LOADED = '[Tenant] Loaded'
}

export const tenantListLoaded = createAction(
  TenantActionTypes.LIST_LOADED,
  props<{ result: Tenant[] }>()
);

export const tenantLoaded = createAction(
  TenantActionTypes.LOADED,
  props<{ result: Tenant }>()
);

export const loadTenantList = createAction(
  TenantActionTypes.LOAD_LIST
);

export const loadTenantListQuietly = createAction(
  TenantActionTypes.LOAD_LIST_QUIET
);

export const loadTenant = createAction(
  TenantActionTypes.LOAD,
  props<{ id: number }>()
);

export const loadTenantQuietly = createAction(
  TenantActionTypes.LOAD_QUIET
);
