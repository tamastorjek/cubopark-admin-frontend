import {createAction, props} from '@ngrx/store';
import {Meter} from '../../models/meter';

export enum MeterActionTypes {
  LIST_LOADED = '[Meter] Meter List Loaded',
  LOAD_LIST = '[Meter] Load Meter List',
  LOAD_LIST_QUIET = '[Meter] Load Meter List Quietly',
}

export const meterListLoaded = createAction(
  MeterActionTypes.LIST_LOADED,
  props<{ result: Meter[] }>()
);

export const loadMeterList = createAction(
  MeterActionTypes.LOAD_LIST
);

export const loadMeterListQuietly = createAction(
  MeterActionTypes.LOAD_LIST_QUIET
);
