import {createAction, props} from '@ngrx/store';
import {ModalSearchResult} from '../../components/shared/modal-search/modal-search-result.model';

export enum FilterActionTypes {
  SET_TENANT_LIST_SEARCH_TERM = '[Filter] Tenant List Search Term',
  SET_METER_READING_FORM_SELECTION = '[Filter] Meter Reading Form Selection'
}

export const setTenantListSearchTerm = createAction(
  FilterActionTypes.SET_TENANT_LIST_SEARCH_TERM,
  props<{ term: string }>()
);

export const setMeterReadingFormSelection = createAction(
  FilterActionTypes.SET_METER_READING_FORM_SELECTION,
  props<{ selection: ModalSearchResult }>()
);
