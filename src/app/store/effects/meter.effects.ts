import {Injectable} from '@angular/core';
import {createEffect, ofType, Actions} from '@ngrx/effects';
import {EMPTY} from 'rxjs';
import {catchError, map, mergeMap} from 'rxjs/operators';
import {MeterService} from '../../services/meter.service';
import {JsonResponse} from '../../models/json-response';
import {MeterActionTypes} from '../actions/meter.actions';

@Injectable()
export class MeterEffects {

  loadMeterList$ = createEffect(() => this.actions$.pipe(
    ofType(MeterActionTypes.LOAD_LIST),
    mergeMap(() => this.meterService.loadMeters()
      .pipe(
        map((response: JsonResponse) => ({type: MeterActionTypes.LIST_LOADED, result: response.data})),
        catchError(() => EMPTY)
      ))
    )
  );

  loadMeterListQuietly$ = createEffect(() => this.actions$.pipe(
    ofType(MeterActionTypes.LOAD_LIST_QUIET),
    mergeMap(() => this.meterService.loadMeters(true)
      .pipe(
        map((response: JsonResponse) => ({type: MeterActionTypes.LIST_LOADED, result: response.data})),
        catchError(() => EMPTY)
      ))
    )
  );

  constructor(
    private actions$: Actions,
    private meterService: MeterService
  ) {
  }
}
