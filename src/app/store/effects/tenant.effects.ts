import {Injectable} from '@angular/core';
import {createEffect, ofType, Actions} from '@ngrx/effects';
import {EMPTY} from 'rxjs';
import {catchError, map, mergeMap} from 'rxjs/operators';
import {TenantService} from '../../services/tenant.service';
import {JsonResponse} from '../../models/json-response';
import {TenantActionTypes} from '../actions/tenant.actions';

@Injectable()
export class TenantEffects {

  loadTenantList$ = createEffect(() => this.actions$.pipe(
    ofType(TenantActionTypes.LOAD_LIST),
    mergeMap(() => this.tenantService.loadTenants()
      .pipe(
        map((response: JsonResponse) => ({type: TenantActionTypes.LIST_LOADED, result: response.data})),
        catchError(() => EMPTY)
      ))
    )
  );

  loadTenantListQuietly$ = createEffect(() => this.actions$.pipe(
    ofType(TenantActionTypes.LOAD_LIST_QUIET),
    mergeMap(() => this.tenantService.loadTenants(true)
      .pipe(
        map((response: JsonResponse) => ({type: TenantActionTypes.LIST_LOADED, result: response.data})),
        catchError(() => EMPTY)
      ))
    )
  );

  loadTenant$ = createEffect(() => this.actions$.pipe(
    ofType(TenantActionTypes.LOAD),
    mergeMap((tenantId) => this.tenantService.loadTenant(tenantId)
      .pipe(
        map((response: JsonResponse) => ({type: TenantActionTypes.LOADED, result: response.data})),
        catchError(() => EMPTY)
      ))
    )
  );

  loadTenantQuietly$ = createEffect(() => this.actions$.pipe(
    ofType(TenantActionTypes.LOAD_QUIET),
    mergeMap((tenantId) => this.tenantService.loadTenant(tenantId, true)
      .pipe(
        map((response: JsonResponse) => ({type: TenantActionTypes.LOADED, result: response.data})),
        catchError(() => EMPTY)
      ))
    )
  );

  constructor(
    private actions$: Actions,
    private tenantService: TenantService
  ) {
  }
}
